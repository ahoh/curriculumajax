$(document).ready(function () {
  var getUserInfo = {
    url: 'https://randomuser.me/api/',
    dataType: 'json'
  };

  // hacer llamada jquery
  $.ajax(getUserInfo)
    .done(function(user){
      console.log('Llamada completada', user);
      var usuario = user.results[0];
      console.log('Objeto del usuario', usuario);

      var nombre = usuario.name.first + " " + usuario.name.last;

      $('#cv_nombre').text(nombre);
      $('#location').text(usuario.location.city);
      $('#location2').text(usuario.location.postcode);
      $('#location3').text(usuario.location.state);
      $('#location4').text(usuario.location.street);
      $('#cv_foto').attr('src', usuario.picture.large);
      $('#contacto_tel').text(usuario.cell);
      $('#contacto_email').text(usuario.email);
      $('#cv_correo').text(usuario.email);
      $('#cv_localizacion').text(usuario.location.city);

    })
    .fail(function(error){
      console.log('llamada con error', error);
    })
});
